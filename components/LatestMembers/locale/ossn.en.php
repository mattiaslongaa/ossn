<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$en = array(
    'com:latestmembers:all:members' => 'All members',
	'com:latestmembers:latest:members' => 'Latest members',
	'com:latestmembers:latest:friends' => 'Latest friends',
);
ossn_register_languages('en', $en); 
