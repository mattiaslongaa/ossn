<?php
/**
 *    OpenSource-SocialNetwork
 *
 * @package   http://www.webbehinds.com
 * @author    Sathish kumar S<info@opensource-socialnetwork.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES 
 * @license   General Public Licence http://opensource-socialnetwork.com/licence
 * @link      http://www.opensource-socialnetwork.com/licence
 */
$settings = new OssnComponents;
$settings = $settings->getComSettings('metatags');
$keywords = $settings->keywords;
$description = $settings->description;
$author = $settings->author;
$robots = $settings->robots;
$revisit = $settings->revisit;

?>
<meta name="keywords" content="<?php echo $keywords?>" />
<meta name="description" content="<?php echo $description ?>" />
<meta name="author" content="<?php echo $author?>" />
<meta name="robots" content="<?php echo $robots ?>" />
<meta name="revisit" content="<?php echo $revisit ?>" />
