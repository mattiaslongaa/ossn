<?php
/**
 *    OpenSource-SocialNetwork
 *
 * @package   (webbehinds.com).ossn
 * @author    OSSN Core Team | Sathish Kumar <info@opensource-socialnetwork.com>
 * @copyright 2014 webbehinds
 * @license   General Public Licence http://opensource-socialnetwork.com/licence
 * @link      http://www.opensource-socialnetwork.com/licence
 */

$OssnComponents = new OssnComponents;

$keywords = input('keywords');
$description = input('description');
$author = input('author');
$robots = input('robots');
$revisit = input('revisit');

$array = array('keywords'=>$keywords,'description'=>$description,'author'=>$author,'robots'=>$robots,'revisit'=>$revisit);
foreach ($array as $key=>$value){
$success = $OssnComponents->setComSETTINGS('metatags', $key, $value);
}
if($success){
    ossn_trigger_message('Settings saved');
    redirect(REF);
} else {
    ossn_trigger_message('Cannot save settings', 'error');
    redirect(REF);
}



