<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Claus Lampert <mai@charttec.de>
 * @copyright 2016 Claus Lampert Finanzinformationen
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 * Credits (most of components-sourcecode):
 * Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 */

/**
 * Shows the search-slot for members and groups
 * Credit to Arsalan Shah for code
 *
 */
 
$component = new OssnComponents;
$settings  = $component->getSettings('SearchSidebar');
echo search_sidebar_output($settings->free_search);
echo ossn_view_form("search", array( "component" => "OssnSearch", "class" => "ossn-search", "autocomplete" => "off", "method" => "get", "security_tokens" => false, "action" => ossn_site_url("search")), false);

/**
 * Not so smart, have to find better solution:
 */
echo "<br>";