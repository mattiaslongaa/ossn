Footer Links
============

V1.0
This component allows you to add your facebook and twitter urls to the line of footer links.
Besides that, one custom link of your choice may be added, too.

Starting from your Ossn Administrator panel, navigate to 'Configure->Footer Links'
and enter the urls of your choice. Or leave the fields blank in order to remove
former links or bypass new settings.

Both facebook and twitter links come with predefined small icons derived from Font Awesome
(http://fontawesome.io/icons/),
for the custom link you have to choose an own meaningful name.