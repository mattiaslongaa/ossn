<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
?>
<br />
<label><?php echo ossn_print('com:footer-links:facebook:url:label'); ?></label>
<input type="text" name="facebook_url" value="<?php echo $params['settings']->facebook_url;?>"/>
<label><?php echo ossn_print('com:footer-links:twitter:url:label'); ?></label>
<input type="text" name="twitter_url" value="<?php echo $params['settings']->twitter_url;?>"/>
<hr>
<label><?php echo ossn_print('com:footer-links:custom:url:label'); ?></label>
<input type="text" name="custom_url" value="<?php echo $params['settings']->custom_url;?>"/>
<label><?php echo ossn_print('com:footer-links:custom:name:label'); ?></label>
<input type="text" name="custom_name" value="<?php echo $params['settings']->custom_name;?>"/>
<br />
<?php echo ossn_print('com:footer-links:instruction'); ?>
<br />
<br />
<input type="submit" value="<?php echo ossn_print("save");?>" class="btn btn-success"/>
