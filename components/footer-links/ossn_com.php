<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

define('__FOOTER_LINKS__', ossn_route()->com . 'footer-links/');

function footer_links_init() {
	// front end
	$component = new OssnComponents;
	$settings = $component->getComSettings('footer-links');
	if ($settings->facebook_url && !empty($settings->facebook_url)) {
		ossn_register_menu_link('facebook', '<i class="fa fa-facebook-square"></i>', $settings->facebook_url, 'footer');
	}
	if ($settings->twitter_url && !empty($settings->twitter_url)) {
		ossn_register_menu_link('twitter', '<i class="fa fa-twitter-square"></i>', $settings->twitter_url, 'footer');
	}
	if ($settings->custom_url && !empty($settings->custom_url)) {
		ossn_register_menu_link('custom', $settings->custom_name, $settings->custom_url, 'footer');
	}
	// back end
	if(ossn_isAdminLoggedin()){
		ossn_register_com_panel('footer-links', 'settings');
		ossn_register_action('footer-links/admin/settings', __FOOTER_LINKS__ . 'actions/footer-links/admin/settings.php');		
	}	
}

ossn_register_callback('ossn', 'init', 'footer_links_init');
