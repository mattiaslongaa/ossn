<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'noparent_npzblog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'nwdt0vnzbundwqloh2bbbmvuneahgrorgnuoctnqxtbkl7ymfcgjfehdomohxroi');
define('SECURE_AUTH_KEY',  'ttl21ajrpucbawezr0ijooakgqfgbutm3embcdkpk3zgeggpbvdhk1gufvotsgcj');
define('LOGGED_IN_KEY',    'ac8vymf6ojrgrrqfp2dezvcxvigblvquixumxcyvctftlycvik584bjtxznsprlk');
define('NONCE_KEY',        'mp22k3ci9pgcu7i4jbu6jkfadubkuxtdfiefuitk9wkatbyoa7cytqgbuve1ftqe');
define('AUTH_SALT',        'grop3c5uov3wkwc4n2os3ri7uk8jc6snpid155ps8ysymeae8xspekqolqlxuarq');
define('SECURE_AUTH_SALT', 'jedc4vrx2ps4ba6wafp0wr8qfsm3vtclzi5yk1rkgkdziahtzveqyck2a2emxir0');
define('LOGGED_IN_SALT',   '9mqg58cydd6kqlggknl1donmipyzcrm4quybd6wljmg9vh0ymcm1dx6esmh0lnm3');
define('NONCE_SALT',       's2fhlicp20esyyoh8ziny274rojf4b7r8gtmr0dfqkibuzbf149kihawxzycfruw');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'npzblogtable';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
