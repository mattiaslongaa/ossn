<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage FB Newsroom
 * @since FB Newsroom 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

    <header id="header-primary">
        <div class="wrapper clearfix" id="header-1">
            
			<div id="logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="logo-icon" title="<?php bloginfo( 'name' ); ?>">
				<img src="<?php print _akov_header_image() ?>" alt="">
				</a>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="logo-text"><?php bloginfo( 'name' ); ?></a>
			</div>

            </div>
        </div><!--end:.wrapper-->

        <div id="header-2">
            <nav class="wrapper">
                <div class="search-box">
					<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
						<label for="search"><span>Search..</span></label> <input id="search" name="s"
						placeholder="Search.." type="text" value="">
					</form>  
                </div><!--end:.search-box-->

                <ul>
                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
                </ul>
				
				
				
            </nav>
        </div><!--end:.wrapper-->
    </header><!--end:.header-primary-->

    <div class="wrapper">
        <h1 class="page-title">Facebook Newsroom</h1>
